package es.cabsa.javadevelopers.es.cabsa.javadevelopers.dto;

import java.io.Serializable;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import es.cabsa.javadevelopers.model.AnimalModel;
import es.cabsa.javadevelopers.model.FoodModel;
import es.cabsa.javadevelopers.model.template.AnimalsInterface;
import es.cabsa.javadevelopers.services.AnimalService;
import es.cabsa.javadevelopers.services.FoodService;

@Component
public class AlimentDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String animalName;
	private String foodName;
	
	public AlimentDTO() {
		super();
	}
	
	public AlimentDTO(String animalName, String foodName) {
		this.animalName = animalName;
		this.foodName = foodName;
	}
	
	public JSONArray getAliments() {
		AnimalService animalService = new AnimalService();
		List<AnimalModel> animals = animalService.getAnimals();
		JSONArray jsonArray = new JSONArray();
		JSONObject json;
		for (int i = 0; i < animals.size(); i++) {
			json = new JSONObject();
			json.put("name", animals.get(i).getName());
			json.put("eats", animals.get(i).getFoodId().getName());
			jsonArray.put(json);
		}
		return jsonArray;
	}
	
	public List<AnimalModel> searchAnimalName(String animalName) {
		AnimalService animalService = new AnimalService();
		List<AnimalModel> animals = animalService.getAnimalSearch(animalName);
		return animals;
	}
	
	public List<FoodModel> searchFoodName(String foodName) {
		FoodService foodService = new FoodService();
		List<FoodModel> food = foodService.getFoodSearch(foodName);
		return food;
	}

}
