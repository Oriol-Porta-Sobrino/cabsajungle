package es.cabsa.javadevelopers.services;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import es.cabsa.javadevelopers.model.AnimalModel;
import es.cabsa.javadevelopers.services.template.AnimalServiceInterface;

@Service
@Component
public class AnimalService implements AnimalServiceInterface {
	

	private static final long serialVersionUID = 1L;
	private List<AnimalModel> animalList = new CopyOnWriteArrayList<AnimalModel>();
	SessionFactory sessionFactory = HibernateSession.getSessionFactory();
	
	public AnimalService() {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		try {
		animalList = (List<AnimalModel>) session.createQuery("from AnimalModel", AnimalModel.class).list();
		
		session.getTransaction().commit();
		
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		} finally {
			session.close();
		}
	}
	
	@Override
	public List<AnimalModel> getAnimalSearch(String animalName) {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		try {
			List<AnimalModel> animalSearch = (List<AnimalModel>) session.createQuery("FROM AnimalModel WHERE name LIKE '%"+ animalName +"%'", AnimalModel.class).list();
		
			session.getTransaction().commit();
			return animalSearch;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		} finally {
			session.close();
		}
		return null;
	}
	
	@Override
	public List<AnimalModel> getAnimals() {
		return animalList;
	}

}
