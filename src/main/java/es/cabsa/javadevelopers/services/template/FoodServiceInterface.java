package es.cabsa.javadevelopers.services.template;

import java.io.Serializable;
import java.util.List;

import es.cabsa.javadevelopers.model.FoodModel;

public interface FoodServiceInterface extends Serializable {
	
	public List<FoodModel> getFood();
	
	public List<FoodModel> getFoodSearch(String foodName);
	
	public void addFood(String nameFood);

}
