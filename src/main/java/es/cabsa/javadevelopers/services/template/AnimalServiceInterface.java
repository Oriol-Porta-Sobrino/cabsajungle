package es.cabsa.javadevelopers.services.template;

import java.io.Serializable;
import java.util.List;

import es.cabsa.javadevelopers.model.AnimalModel;

public interface AnimalServiceInterface extends Serializable {
	
	public List<AnimalModel> getAnimalSearch(String foodName);
	
	public List<AnimalModel> getAnimals();

}
