package es.cabsa.javadevelopers.services;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import es.cabsa.javadevelopers.model.AnimalModel;
import es.cabsa.javadevelopers.model.FoodModel;
import es.cabsa.javadevelopers.services.template.FoodServiceInterface;

@Service
@Component
public class FoodService implements FoodServiceInterface {
	
	private static final long serialVersionUID = 1L;
	private List<FoodModel> foodList = new CopyOnWriteArrayList<FoodModel>();
	SessionFactory sessionFactory = HibernateSession.getSessionFactory();
	
	public FoodService() {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		try {
			foodList = (List<FoodModel>) session.createQuery("from FoodModel", FoodModel.class).list();
		
		session.getTransaction().commit();
		
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		} finally {
			session.close();
		}
	}
	
	@Override
	public List<FoodModel> getFoodSearch(String foodName) {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		try {
			List<FoodModel> foodSearch = (List<FoodModel>) session.createQuery("FROM FoodModel WHERE name LIKE '%"+ foodName +"%'", FoodModel.class).list();
		
			session.getTransaction().commit();
			return foodSearch;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();

		} finally {
			session.close();
		}
		return null;
	}
	
	@Override
	public void addFood(String nameFood) {

		FoodModel food = new FoodModel(foodList.size()+1, nameFood);
	    Session session = sessionFactory.getCurrentSession();
	    try {
	    	session.beginTransaction();
		    session.save(food);
		    session.getTransaction().commit();
	    } catch (HibernateException e) {
	    	e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
	    } finally {
			session.close();
		}
	    
	}

	@Override
	public List<FoodModel> getFood() {
		return foodList;
	}

}
