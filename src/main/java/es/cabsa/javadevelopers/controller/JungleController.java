package es.cabsa.javadevelopers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.cabsa.javadevelopers.es.cabsa.javadevelopers.dto.AlimentDTO;
import es.cabsa.javadevelopers.services.AnimalService;
import es.cabsa.javadevelopers.services.FoodService;

@Controller
public class JungleController {
	

	  @RequestMapping("/")
	  public String index(ModelMap model) {
		  AlimentDTO dto = new AlimentDTO();
		  String prueba = "prueba";
		  model.addAttribute("prueba", prueba);
		  model.addAttribute("json", dto.getAliments().toString());
	    return "index";
	  }
	  
	  @RequestMapping("show")
	  public String json(Model model) {
		  AlimentDTO dto = new AlimentDTO();
		  String prueba = "prueba";
		  model.addAttribute("prueba", prueba);
		  //model.addAttribute("json", dto.getAliments().toString());
		  return dto.getAliments().toString();
		  
	  }
  
  

}
