package es.cabsa.javadevelopers.model.template;

public interface AnimalsInterface {
	
	public int getId();
	
	public String getName();
	
	public void setName(String name);
	
	public int getLegs();
	
	public void setLegs(int legs);

}
