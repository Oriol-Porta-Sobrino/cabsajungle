package es.cabsa.javadevelopers.model.template;

public interface FoodInterface {
	
	public int getId();
	
	public String getName();
	
	public void setName(String name);

}
