package es.cabsa.javadevelopers.model;

import javax.persistence.*;

import org.springframework.stereotype.Component;

import es.cabsa.javadevelopers.model.template.AnimalsInterface;

@Entity
@Table(name="animal")
public class AnimalModel implements AnimalsInterface {
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "name")
	private String animalName;
	
	@Column(name = "legs")
	private int animalLegs;
	
	@ManyToOne
	@JoinColumn(name = "foodId")
	private FoodModel foodId;
	

	public AnimalModel() {
		super();
	}
	
	public AnimalModel(int id, String animalName, int animalLegs) {
		super();
		this.id = id;
		this.animalName = animalName;
		this.animalLegs = animalLegs;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		return animalName;
	}

	@Override
	public void setName(String animalName) {
		this.animalName = animalName;
	}

	@Override
	public int getLegs() {
		return animalLegs;
	}

	@Override
	public void setLegs(int animalLegs) {
		this.animalLegs = animalLegs;
	}
	
	public FoodModel getFoodId() {
		return foodId;
	}

	public void setFoodId(FoodModel foodId) {
		this.foodId = foodId;
	}

	@Override
	public String toString() {
		return "AnimalModel [id=" + id + ", animalName=" + animalName + ", animalLegs=" + animalLegs + ", foodId="
				+ foodId.getName() + "]";
	}

}
