package es.cabsa.javadevelopers.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import org.springframework.stereotype.Component;

import es.cabsa.javadevelopers.model.template.FoodInterface;

@Entity
@Table(name="food")
public class FoodModel implements FoodInterface {
	
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name="name")
	private String foodName;
	
	@OneToMany(mappedBy = "foodId", fetch = FetchType.EAGER)
	private Set<AnimalModel> animal = new HashSet<AnimalModel>();
	
	public FoodModel() {
		super();
	}

	public FoodModel(int id, String foodName) {
		super();
		this.id = id;
		this.foodName = foodName;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getName() {
		return foodName;
	}

	@Override
	public void setName(String foodName) {
		this.foodName = foodName;
	}
	
	public Set<AnimalModel> getAnimal() {
		return animal;
	}

	public void setAnimal(Set<AnimalModel> animal) {
		this.animal = animal;
	}

	@Override
	public String toString() {
		return "FoodModel [id=" + id + ", foodName=" + foodName + ", animal=" + animal + "]";
	}


}
