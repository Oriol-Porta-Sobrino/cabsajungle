package es.cabsa.javadevelopers;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import es.cabsa.javadevelopers.es.cabsa.javadevelopers.dto.AlimentDTO;
import es.cabsa.javadevelopers.services.AnimalService;
import es.cabsa.javadevelopers.services.FoodService;

@SpringBootApplication
public class Application {
	
	private static Scanner sc;

  public static void main(String[] args) {
    ApplicationContext ctx = SpringApplication.run(Application.class, args);

    System.out.println("Animals in the jungle:");
    
    sc = new Scanner(System.in);
     
    AlimentDTO dto = new AlimentDTO();
    
    System.out.println(dto.getAliments().toString());
    
    System.out.println("Search an animal by name: ");
    
    String animalName = sc.nextLine();
    
    System.out.println(dto.searchAnimalName(animalName).toString());
    
    System.out.println("Search food by name: ");
    
    String foodName = sc.nextLine();
    
    System.out.println(dto.searchFoodName(foodName));
    
    System.out.println("Add new food");
    System.out.println("Food name");
    
    String newFood = sc.nextLine();
    
    FoodService foodService = new FoodService();
    
    foodService.addFood(newFood);
    
    System.out.println("Insert succesfull");
    
    sc.close();
    

    /*String[] beanNames = ctx.getBeanDefinitionNames();
    Arrays.sort(beanNames);
    for (String beanName : beanNames) {
      System.out.println(beanName);
    }*/
  }
  

		

}
